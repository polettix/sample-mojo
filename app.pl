#!/usr/bin/env perl
use Mojolicious::Lite;
our $VERSION = $ENV{VERSION} || '0.1';
get '/' => sub {
   my $c = shift;
   $c->res->headers->content_type('text/plain');
   $c->render(text => "Hello, World! ($VERSION)\n");
};
app->start;
